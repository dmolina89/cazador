<?php
echo'
			void setup() {
			  background(0);
			  size(550,400);
			  AleatoriMenjar();
			}
			void draw() {
			  background(0);
			  fill(255,0,0);
			  rect(food[0],food[1],10,10);
			  fill(0,255,0);
			  rect(jugador[0],jugador[1],10,10);
			  fill(255);
			  rect(400,-1,10,401);
			  subida_nivel();
			  text("Nivel: " +nivel, 420, 20);
			  text("Score: " +score, 420, 40);
			  text("Energia: ", 420,80);
			  fill(255,255,0);
			  rect(420,90,energia/2,10);
			  fill(153,153,153);
			  rect(p1[0],p1[1], 10, 10);
			  rect(p2[0],p2[1], 10, 10);
			  rect(p3[0],p3[1], 10, 10);
			  rect(p4[0],p4[1], 10, 10);
			  historia();
			  if (!pause) {
				movimiento();
				document.getElementById("cuerpo").play();
			  } else {
			  	document.getElementById("cuerpo").pause();
				if (gameover) {
					if (gameover_sound) {
				 	 document.getElementById("overgame").play();
				 	 gameover_sound=!gameover_sound
				 	}
				  text(" G A M E      O V E R ", 150, 200);
				} else {
				  text("PAUSA", 180, 200);
				}
			  }
			  if (menja()) {
				score=score+1;
				food[0]=random(10,400)-10;
				food[1]=random(10,400)-10;
				if (score%10==0 && score!=0) {
					vel=vel+0.25;
					nivel=nivel+1;
				}
				while (aliment(p1)||aliment(p2)||aliment(p3)||aliment(p4)) {
				  AleatoriMenjar();
				}
			  }
			  colision(p1);
			  colision(p2);
			  colision(p3);
			  colision(p4);
			}
			';
?>