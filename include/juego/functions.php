<?php
echo'
			boolean menja() {
			  if (jugador[0]+8 < food[0]) {
				  return false;
				}
				if (jugador[1]+8 < food[1]) {
				  return false;
				}
				if (jugador[0]>food[0]+8) {
				  return false;
				}
				if (jugador[1]>food[1]+8) {
				  return false;
				}
				return true;
			}
			void coenem(float[] enemigo) {
			    if (enemigo[0]+8 < food[0]) {
			        return false;
			    } else if (enemigo[1]+8 < food[1]) {
			        return false;
			    } else if (enemigo[0]>food[0]+8) {
			        return false;
			    } else if (enemigo[1]>food[1]+8) {
			        return false;
			    }
			    return true;
			}
			boolean xoca(float[] pared) {
			  if (jugador[0]+9 < pared[0]) {
				  return false;
				}
				if (jugador[1]+9 < pared[1]) {
				  return false;
				}
				if (jugador[0]>pared[0]+9) {
				  return false;
				}
				if (jugador[1]>pared[1]+9) {
				  return false;
				}
				return true;
			}
			void movimiento() {
				accelerar();
				if (move==1) {
					jugador[0]=jugador[0]+vel;
				} else if (move==2) {
					jugador[0]=jugador[0]-vel;
				} else if (move==3) {
					jugador[1]=jugador[1]-vel;
				} else if (move==4) {
					jugador[1]=jugador[1]+vel;
				}
				direccion();
				limites();
			}
			void accelerar() {
				if(keyCode==18 && keyPressed) {
					if(energia>5) {
						vel=4;
						energia=energia-5;
					} else {
						vel=1+(nivel-1)*0.25;
					}
				} else if(energia<200) {
					vel=1+(nivel-1)*0.25;
					energia=energia+1;
				}

			}
			void direccion() {
				if (keyPressed) {
					if (keyCode==40 || key=="s") {
						move=4;
					} else if (keyCode==38 || key=="w") {
						move=3;
					} else if (keyCode==39 || key=="d") {
						move=1;
					} else if (keyCode==37 || key=="a") {
						move=2;
					}
			  }
			}
			void limites() {
				if (jugador[0]>400) {
					jugador[0]=1;
				}else if (jugador[0]<0) {
					jugador[0]=399;
				} else if (jugador[1]>400) {
					jugador[1]=1;
				} else if (jugador[1]<0) {
					jugador[1]=399;
				}
			}
			void keyPressed() {
			  if(keyCode==10 || keyCode==32) {
				pause = !pause;
				document.getElementById("inicio").play();
				if (gameover) {
					gameover=!gameover;
					gameover_sound=!gameover_sound
				}
			  }
			}
			void colision(float[] pared) {
				if (xoca(pared)) {
					if (score>maxScore) {
					  maxScore=score;
					  document.enviar.rank.value=maxScore;
					}
					reset();
				  }
			}
			boolean aliment(float[] pared) {
			  if (food[0]+9 < pared[0]) {
				  return false;
				}
				if (food[1]+9 < pared[1]) {
				  return false;
				}
				if (food[0]>pared[0]+9) {
				  return false;
				}
				if (food[1]>pared[1]+9) {
				  return false;
				}
				return true;
			}
			void subida_nivel() {
			if (nivel>1) {
			  	invoca(p5);
			  	if(nivel>2) {
			  		invoca(p6);
			  		if(nivel>3) {
			  			invoca(p7);
			  			if(nivel>4) {
			  				invoca(p8);
			  				if (nivel>5 && !pause) {
			  					mouEnemicX(p5);
			  					mouEnemicX(p6);
			  					mouEnemicY(p7);
			  					mouEnemicY(p8);
			  					if (nivel>6) {
			  						invoca(p9);
			  						invoca(p10);
			  						invoca(p11);
			  						invoca(p12);
			  						if (nivel>7 && !pause) {
			  							mouEnemicY(p9);
			  							mouEnemicY(p10);
			  							mouEnemicX(p11);
			  							mouEnemicX(p12);
                                        if (nivel>=8) {
                                            fill(153,153,153);
                                            float[] central = {190,190,20,20};
                                            rect(central[0],central[1],central[2], central[3]);
                                            coenem(central);
                                            zonaMorta(185,205);
                                        }
			  					}
			  				}
			  			}
			  		}
			  	}
			  }
			  patro_enemic1(p5);
			  patro_enemic1(p6);
			  patro_enemic1(p7);
			  patro_enemic1(p8);
			  patro_enemic2(p9);
			  patro_enemic2(p10);
			  patro_enemic2(p11);
			  patro_enemic2(p12);
			 }
			 }
			 void patro_enemic1(float[] pared) {
			  if (pared[0]==285||pared[1]==285) {
			  	pared[2]=-1;
			  }
			  if (pared[0]==105||pared[1]==105) {
			  	pared[2]=1;
			  }
			 }
			 void patro_enemic2(float[] pared) {
			 	if (pared[0]>390 || pared[1]>390) {
			 		pared[2]=-1;
			 	}
			 	if (pared[0]<1 || pared[1]<1) {
			 		pared[2]=1;
			 	}
			 }
			 void reset() {
				score=0;
				jugador[0]=200;
				jugador[1]=200;
				gameover=true;
				pause=true;
				nivel=1;
				p5={110,100};
				p6={290,300};
				p7={300,110};
				p8={100,290};
				vel=1;
				energia=200;
			 }
			 void historia() {
				 switch(nivel) {
				 	case 1: element.innerHTML=historia1;
				 		break;
				 	case 2: element.innerHTML=historia2;
				 		break;
				 	case 3: element.innerHTML=historia3;
				 	    break;
				 	case 4: element.innerHTML=historia4;
				 	    break;
				 	case 5: element.innerHTML=historia5;
				 	    break;
				 	case 6: element.innerHTML=historia6;
				 	    break;
				 	case 7: element.innerHTML=historia7;
				 	    break;
				 	case 8: element.innerHTML=historia8;
				 	    break;
				 	case 9: element.innerHTML=historia9;
				 }
			}
			voic dibuixEnemic(float[] xy) {
			    rect(xy[0],xy[1],10,10);
			}
			void AleatoriMenjar() {
			    food[0]=random(0,400)-1;
			    food[1]=random(0,400)-1;
			}
			void mouEnemicX(float[] enemic) {
			    enemic[0]=enemic[0]+enemic[2];
			}
			void mouEnemicY(float[] enemic) {
			    enemic[1]=enemic[1]+enemic[2];
			}
			void invoca(float[] enemic) {
			    dibuixEnemic(enemic);
                colision(enemic);
                MenjarEnemic(enemic);
			}
			void MenjarEnemic(float[] enemic) {
			    if (coenem(enemic)) {
			        AleatoriMenjar();
			    }
			}
			void zonaMorta(int min, int max) {
			    if ((jugador[0]>min-10&&jugador[0]<max) && (jugador[1]>min-10&&jugador[1]<max)) {
					if (score>maxScore) {
					  maxScore=score;
					  document.enviar.rank.value=maxScore;
					}
					reset();
				  }
			}
			';
?>