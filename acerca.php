<!DOCTYPE html>
<html lang="es">
<head>
    <title>Cazador</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <link rel="stylesheet" href="css/style.css" />
    <!-- Librerias HTML5 par  ie8 -->
    <!--[if lt IE9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.42.2/respond.min.js"></script>
    <![endif]-->
    <!-- JavaScript para libreria processing -->
    <script type="text/javascript" src="processing.js"></script>
    <!-- JS para historia -->
    <script type="text/javascript" src="js/vars.js"></script>
</head>
<body>
<?php include_once 'include/analytics.php'; ?>
<!-- Jquery -->
<script src="http://code.jquery.com/jquery.js"></script>
<!-- Plugins -->
<script src="js/bootstrap.min.js"></script>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Menu desplegable</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Cazador</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Inicio</a></li>
                <li class="active"><a href="acerca.php">Acerca</a></li>
                <!--<li><a href="#contact">Contact</a></li>-->
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <article id="historia">

            </article>
        </div>
        <div class="col-md-6">
            <script type="application/processing">
            float[] p1={95,95};
			float[] p2={95,295};
			float[] p3={295,95};
			float[] p4={295,295};
			float[] p5={105,95,1};
			float[] p6={285,295,-1};
			float[] p7={295,105,1};
			float[] p8={95,285,-1};
			float[] p9={195,195,1};
			float[] p10={195,195,-1};
			float[] p11={195,195,-1};
			float[] p12={195,195,1};
			 <?php
                switch ($_GET["nivel"]) {
                    case 1: echo 'int nivel=1;';
                        break;
                    case 2: echo 'int nivel=2;';
                        break;
                    case 3: echo 'int nivel=3;';
                        break;
                    case 4: echo 'int nivel=4;';
                        break;
                    case 5: echo 'int nivel=5;';
                        break;
                    case 6: echo 'int nivel=6;';
                        break;
                    case 7: echo 'int nivel=7;';
                        break;
                    case 8: echo 'int nivel=8;';
                        break;
                    case 9: echo 'int nivel=9;';
                        break;
                    case 10: echo 'int nivel=10;';
                        break;
                }
                ?>
			void setup() {
			    background(0);
			    size(400,400);
			}
			void draw() {
			    fill(153);
			    rect(p1[0],p1[1],10,10);
			    rect(p2[0],p2[1],10,10);
			    rect(p3[0],p3[1],10,10);
			    rect(p4[0],p4[1],10,10);
			    fill(255);
                if (nivel>1) {
                    rect(p5[0],p5[1],10,10);
                    if (nivel>2) {
                        rect(p6[0],p6[1],10,10);
                        if (nivel>3) {
                            rect(p7[0],p7[1],10,10);
                            if(nivel>4) {
                                rect(p8[0],p8[1],10,10);
                                if(nivel>5) {
                                    mouEnemicX(p5);
                                    mouEnemicX(p6);
                                    mouEnemicY(p7);
                                    mouEnemicY(p8);
                                    if(nivel>6) {
                                        rect(p9[0],p9[1],10,10);
                                        rect(p10[0],p10[1],10,10);
                                        rect(p11[0],p11[1],10,10);
                                        rect(p12[0],p12[1],10,10);
                                        if(nivel>7) {
                                            mouEnemicY(p9);
                                            mouEnemicY(p10);
                                            mouEnemicX(p11);
                                            mouEnemicX(p12);
                                            if(nivel>=8) {
                                                fill(153,153,153);
                                                float[] central = {190,190,20,20};
                                                rect(central[0],central[1],central[2], central[3]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                patro_enemic1(p5);
			    patro_enemic1(p6);
			    patro_enemic1(p7);
			    patro_enemic1(p8);
			    patro_enemic2(p9);
			    patro_enemic2(p10);
			    patro_enemic2(p11);
			    patro_enemic2(p12);

            }
            void mouEnemicX(float[] enemic) {
                enemic[0]=enemic[0]+enemic[2];
            }
            void mouEnemicY(float[] enemic) {
                enemic[1]=enemic[1]+enemic[2];
            }
            void patro_enemic1(float[] pared) {
			  if (pared[0]==285||pared[1]==285) {
			  	pared[2]=-1;
			  }
			  if (pared[0]==105||pared[1]==105) {
			  	pared[2]=1;
			  }
			}
            void patro_enemic2(float[] pared) {
                if (pared[0]>390 || pared[1]>390) {
                    pared[2]=-1;
                }
                if (pared[0]<1 || pared[1]<1) {
                    pared[2]=1;
                }
            }
            </script>
            <canvas id="processingCanvas" width="100%"></canvas>
            <ul class="pagination">
                <li <?php if($_GET["nivel"]==1) echo 'class=active' ?>>
                    <a href="acerca.php?nivel=1">1</a>
                </li>
                <li <?php if($_GET["nivel"]==2) echo 'class=active' ?>>
                    <a href="acerca.php?nivel=2">2</a>
                </li>
                <li <?php if($_GET["nivel"]==3) echo 'class=active' ?>>
                    <a href="acerca.php?nivel=3">3</a>
                </li>
                <li <?php if($_GET["nivel"]==4) echo 'class=active' ?>>
                    <a href="acerca.php?nivel=4">4</a>
                </li>
                <li <?php if($_GET["nivel"]==5) echo 'class=active' ?>>
                    <a href="acerca.php?nivel=5">5</a>
                </li>
                <li <?php if($_GET["nivel"]==6) echo 'class=active' ?>>
                    <a href="acerca.php?nivel=6">6</a>
                </li>
                <li <?php if($_GET["nivel"]==7) echo 'class=active' ?>>
                    <a href="acerca.php?nivel=7">7</a>
                </li>
                <li <?php if($_GET["nivel"]==8) echo 'class=active' ?>>
                    <a href="acerca.php?nivel=8">8</a>
                </li>
                <li <?php if($_GET["nivel"]==9) echo 'class=active' ?>>
                    <a href="acerca.php?nivel=9">9</a>
                </li>
                <li <?php if($_GET["nivel"]==10) echo 'class=active' ?>>
                    <a href="acerca.php?nivel=10">10</a>
                </li>
            </ul>
        </div>
        <div class="col-md-3">
            <article id="description">

            </article>
        </div>
    </div>
</div>
<?php include_once 'include/footer.php'; ?>
</body>
</html>