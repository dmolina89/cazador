<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Cazador</title>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!-- CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" media="screen" />
		<link rel="stylesheet" href="css/style.css" />
		<!-- Librerias HTML5 par  ie8 -->
		<!--[if lt IE9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.42.2/respond.min.js"></script>
		<![endif]-->
		<!-- JavaScript para libreria processing -->
		<script type="text/javascript" src="processing.js"></script>
		<!-- JS para historia -->
		<script type="text/javascript" src="js/vars.js"></script>
	</head>
	<body>
	<?php include_once 'include/analytics.php'; ?>
		<!-- Jquery -->
		<script src="http://code.jquery.com/jquery.js"></script>
		<!-- Plugins -->
		<script src="js/bootstrap.min.js"></script>
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Menu desplegable</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Cazador</a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li class="active"><a href="index.php">Inicio</a></li>
						<li><a href="acerca.php?nivel=1">Acerca</a></li>
						<!--<li><a href="#contact">Contact</a></li>-->
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</nav>
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<article id="historia">

					</article>
				</div>
				<div class="col-md-6">
					<?php require_once("include/game.php"); ?>
					<canvas id="processingCanvas" width="100%"></canvas>
					<audio id="inicio" src="sound/inicio.ogg"></audio>
					<audio id="overgame" src="sound/game_over.wav"></audio>
					<audio id="cuerpo" src="sound/cuerpo.wav"></audio>
				</div>
				<div class="col-md-3">
					<article id="description">
						<h2>Instrucciones</h2>
						<p>Persique a tus presas (recuadro rojo) esquivando todos los obstaculos (recuadros grises) y ten mucho cuidado con los enemigos te pueden atacar a ti... ¡son muy peligrosas!</p>
						<h2>Controles</h2>
						<ul>
							<li>&uarr; o W: Arriba</li>
							<li>&rarr; o D: Derecha</li>
							<li>&larr; o A: Izquierda</li>
							<li>&darr; o S: Abajo</li>
							<li>Alt o Alt Gr: Sprint</li>
							<li>Enter o Espacio: start/pausa</li>
						</ul>
					</article>
				</div>
			</div>
		</div>
		<?php
		include_once 'include/formulari.php';
		include_once 'include/connect.php';
		include_once 'include/cookies.php';
		if(isset($_POST["nom"])&&isset($_POST["rank"])) {
			if ($_POST["nom"]!=null) {
				$nom=$_POST["nom"];
				$rank=$_POST["rank"];
				$rank2=$_POST["rank2"];
				$level=$_POST["level"];
				$level=$level*10;
				$consulta="SELECT Puntos FROM ranking WHERE Nick='";
				$consulta .= $nom;
				$consulta .="'";
				$resultat=mysqli_query($enlace, $consulta);
				$total=mysqli_num_rows($resultat);
				if($level<$rank && $rank==$rank2) {
					if ($total == 0) {
						$insert = "INSERT INTO ranking VALUES ('";
						$insert .= $nom;
						$insert .= "','";
						$insert .= $rank;
						$insert .= "')";
						$query = mysqli_query($enlace, $insert);
						if (!$query) {
							die($insert);
						}
					} else {
						$total = mysqli_fetch_array($resultat);
						if ($total[0] <= $rank) {
							$update = "UPDATE ranking SET Puntos=";
							$update .= $rank;
							$update .= " WHERE Nick='" . $nom . "'";
							$query = mysqli_query($enlace, $update);
							if (!$query) {
								die($insert);
							}
						}
					}
				}
			}
		}
			$consulta="SELECT * FROM ranking ORDER BY Puntos DESC";
			$result=mysqli_query($enlace, $consulta);
			echo "<div class='table-responsive'>\n<h2>Ranking</h2>";
			echo "<table class='table table-striped' id='ranking'>\n";
			echo "<thead>\n";
			echo "<tr><th>Puntos</th><th>Nick</th></tr>\n";
			echo "</thead>\n<tbody>\n";
			$contador=0;
			while ($row=mysqli_fetch_array($result)) {
				$r_nom=$row['Nick'];
				$r_punts=$row['Puntos'];
				$contador++;
				echo "<tr><td>";
				echo $r_nom;
				echo "</td><td>";
				echo $r_punts;
				echo "</td></tr>\n";
			}
			echo "</tbody>\n</table>";
			mysqli_close($enlace);
		echo '</section>';
		include_once 'include/footer.php'; ?>
		</body>
</html>